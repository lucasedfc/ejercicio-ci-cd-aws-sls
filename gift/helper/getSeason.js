const getSeason = (month) => {
    if (3 <= month && month <= 5)  return 'Sweater' // autumn
    if (6 <= month && month <= 8)  return 'Buzo'   // winter
    if (9 <= month && month <= 12) return'Camisa' // spring    
    return 'Remera'; // summer
}

module.exports = {
    getSeason
}