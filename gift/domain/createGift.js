//@ts-check
const { getSeason } = require('../helper/getSeason');
const { CreateGiftValidation } = require('../schema/input/createGiftValidation');
const { assignGift } = require('../service/assignGift');

module.exports = async (commandPayload, commandMeta) => {

    const { birthDate, dni } = new CreateGiftValidation(commandPayload, commandMeta).get();
    const month = new Date(birthDate).getMonth();
    const gift = getSeason(month);
    const dbParams = { update: { gift }, dni };

    try {
        const resp = await assignGift(dbParams);
        console.log({ resp });
    } catch (error) {
        console.error(error);
    }

    return { status: 200, body: 'Gift Created' }
}