const randomNumber = (min, max) => {
    return Math.floor(Math.random() * (max - min) + min); 
}

const calculateAge = (birthDate) => {
    const ageDiff = Date.now() - new Date(birthDate).getTime()
    const ageDiffToDateFormat = new Date(ageDiff);
    return Math.abs((ageDiffToDateFormat.getUTCFullYear() -1970));    
}

module.exports = { 
    calculateAge,
    randomNumber 
}